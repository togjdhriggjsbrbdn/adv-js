class Hamburger {
    constructor(size,stuffing){
        try {
            if (size === undefined) {
                throw new HamburgerException('Введите корректные данные')
            }
            if (size.type !== 'size'){
                throw new HamburgerException('Введите размер бургера')
            }
            if (stuffing === undefined) {
                throw new HamburgerException('Введите корректные данные')
            }
            if (stuffing.type !== 'stuffing'){
                throw new HamburgerException('Введите начинку')
            }

            this.size = size;
            this.stuffing = stuffing;
            this.toppings = [];
        } catch (e) {
            console.log(e.message)
        }
    }

    set addTopping (topping){
        try {
            if (topping === undefined){
                throw new HamburgerException('Нету топинга')
            }if(topping.type !== 'topping') {
                throw new HamburgerException('Передан не топинг')
            }
            if (this.toppings.includes(topping)){
                throw new HamburgerException('Есть уже такой топинг')

            }
            this.toppings.push(topping);
            return this.toppings
        }catch (e) {
            console.log(e.message);
        }
    };

    set removeTopping (topping){
        let index = this.toppings.indexOf(topping);

        try {
            if (topping === undefined){
                throw new HamburgerException('Нету топинга')
            }if(topping.type !== 'topping') {
                throw new HamburgerException('Передан не топинг')
            }if (index<0 ){
                throw new HamburgerException('Добавка ранее не была добавлена')
            }
            this.toppings.splice(index, 1)

        }catch ({message}) {
            console.log(message);
        }

    };

    get getToppings(){
        return this.toppings

    };

    get getSize(){
        return this.size.size
    };

    get getStuffing (){
        return this.stuffing.stuffing
    };

    get calculatePrice (){
        const arrayToppings = this.toppings;
        let sumPrice = 0;
        arrayToppings.forEach(elem=> sumPrice +=elem.price);
        this.price = sumPrice + this.size.price + this.stuffing.price;
        return this.price

    };

    get calculateCalories (){
        const arrayToppings = this.toppings;
        let sumCal = 0;
        arrayToppings.forEach(elem => sumCal += elem.cal);
        this.cal = sumCal + this.size.cal + this.stuffing.cal;
        return this.cal
    };




}

Hamburger.SIZE_SMALL = {
    type: 'size',
    size: 'Size Small',
    price: 50,
    cal: 20,
};

Hamburger.SIZE_LARGE = {
    type: 'size',
    size: 'Size Large',
    price: 100,
    cal: 40,
};

Hamburger.STUFFING_CHEESE = {
    type: 'stuffing',
    stuffing: 'Stuffing Cheese',
    price: +10,
    cal: +20,
};

Hamburger.STUFFING_SALAD = {
    type: 'stuffing',
    stuffing: 'Stuffing Salad',
    price: +20,
    cal: +5,
};

Hamburger.STUFFING_POTATO = {
    type: 'stuffing',
    stuffing: 'Stuffing Potato',
    price: +15,
    cal: +10,
};

Hamburger.TOPPING_MAYO = {
    type: 'topping',
    name: 'Mayo',
    price: +20,
    cal: +5,
};

Hamburger.TOPPING_SPICE = {
    type: 'topping',
    name: 'Spice',
    price: +15,
    cal: +0,
};

function HamburgerException (message) {
    this.message = message
}

let burger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);


console.log(burger.getSize);
console.log(burger.getStuffing);
burger.addTopping = Hamburger.TOPPING_MAYO;
burger.addTopping = Hamburger.TOPPING_SPICE;
console.log(burger.getToppings);
burger.removeTopping = Hamburger.TOPPING_MAYO;
console.log(burger.calculatePrice);
console.log(burger.calculateCalories);















